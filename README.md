# [Vite](https://vitejs.dev/)

What is Vite?

- Another method implement create-react-app. You can start a react application faster.

## Vite Install (As of February 2023)

Navigate to a directory where you want to create the react application. Then follow the commands below.

```
npm create vite@latest <NAME-OF-APP> -- --template react
cd <NAME-OF-APP>
npm install
npm run dev
```

After running the commands above, navigate to http://localhost:5173.

## Key differences

- For .js files, you need to use .jsx.
- index.html is in the source directory instead of public.
